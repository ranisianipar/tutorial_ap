package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{

	ModelDuck(){
		flyBehavior = new FlyNoWay();
		quackBehavior = new MuteQuack();
	}
	public void display(){
		System.out.println("DUCK MODEL");
	}
}
